import {Country} from '../country';
import { CountryDetailsComponent } from '../country-details/country-details.component';
import {CountryService} from '../country.service';
import {Component, OnInit, EventEmitter, Output, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {

   @ViewChild('countryDetails') countryDetails: CountryDetailsComponent;
  
  countries: Country[];


  reloadData(): any {
    this.countryService.getCountriesList()
      .subscribe((data) => {
        this.countries = data;
    });
  }

  constructor(private countryService: CountryService, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.reloadData();

  }

  deleteCountry(country: Country): void {
    console.log(country);
    this.countryService.deleteCountry(country)
      .subscribe(data => {
        this.ngOnInit();
      });
  }
  
  updateCountry(country:Country, idCountry: number) {
    this.countryDetails.loadCountryToEdit(country,idCountry);
  }

  processSubmit(boolean) {
    this.ngOnInit();
  }




}
