export class Country {
    id:number;
    countryName: string;
    population: number;

    constructor(id,countryName, population) {
      this.id=id;
      this.countryName = countryName;
      this.population = population;
    }
  
}
