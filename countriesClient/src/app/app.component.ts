
import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { Country } from './country';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryService } from './country.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faGlobeAmericas } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  faGlobe = faGlobeAmericas;

  constructor() { }

  ngOnInit() {

  }



}
