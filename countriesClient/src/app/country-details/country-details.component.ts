import {AppComponent} from '../app.component';
import {Observable} from 'rxjs';
import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';

import {Country} from '../country';
import {CountryListComponent} from '../country-list/country-list.component';
import {CountryService} from '../country.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  populationPattern = '[0-9]*';
  country: Country;
  statusCode: number;
  editCountryMode = false;

  @Output() submit = new EventEmitter<boolean>();


  constructor(private formBuilder: FormBuilder, private countryService: CountryService) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      id: [null],
      countryName: ['', Validators.required],
      population: ['', [Validators.required, Validators.pattern(this.populationPattern)]]
    });

  }

  // convenience getter for easy access to form fields
  get f() {return this.registerForm.controls;}

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    const countryName = this.registerForm.get('countryName').value;
    const population = this.registerForm.get('population').value;
    const id = this.registerForm.get('id').value;


    if (!this.editCountryMode) {
      this.country = new Country(null,countryName, population);
      this.countryService.createCountry(this.country)
        .subscribe(successCode => {
          console.log("Country/Population was added");
          this.submitted = false;
          this.registerForm.reset();
          this.submit.emit(true);
        });
    } else {

      this.country= new Country(id, countryName, population);
      this.countryService.updateCountry(this.country)
        .subscribe(successCode => {
          console.log("Country/Population was updated");
          this.submitted = false;
          this.editCountryMode = false;
          this.registerForm.reset();
          this.submit.emit(true);
        });
    }
  }


  loadCountryToEdit(country: Country, idCountry:number) {
    this.registerForm = this.formBuilder.group({
      id: idCountry,
      countryName: [country.countryName, Validators.required],
      population: [country.population, [Validators.required, Validators.pattern(this.populationPattern)]]
    });
    this.editCountryMode = true;
  }
}
