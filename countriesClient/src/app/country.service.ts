import { Country } from './country';
import { HttpRequest } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable()
export class CountryService {

  private baseUrl = 'http://localhost:8080/countries';

  constructor(private http: HttpClient) { }

  getCountry(id: number) {
    return this.http.get<Country>(`${this.baseUrl}/${id}`);
  }

  createCountry(country: Country) {
    return this.http.post(`${this.baseUrl}`, country, httpOptions);
  }

  updateCountry(country: Country) {
    return this.http.put(`${this.baseUrl}`, country, httpOptions);
  }

  deleteCountry(country) {
     const optionsDel = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: country,
    };
    return this.http.delete(`${this.baseUrl}`, optionsDel);
  }

  getCountriesList() {
    return this.http.get<Country []>(`${this.baseUrl}`);
  }

}
