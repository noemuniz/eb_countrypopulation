
# Country/Population Application
---
This is a project using **Angular 6** and **Spring Boot**. Spring Boot has ben use to expose REST EndPoints and the front-end is written in Angular 6. This is a complete web application with a connectivity to **h2-inmemory database**. 

This project use following versions:

1. Spring Boot v2.0.0
2. Angular v6
3. Maven
4. Node v8.0.4


## Requirements
---

Before running the application, you will need install:

1. Java - 1.8.x
2. Maven
3. Node - v8.11.2


## Steps to Setup
---

**1. Clone the application**

``
    $ git clone https://noemuniz@bitbucket.org/noemuniz/eb_countrypopulation.git
``

**2. Build and run the backend app using maven**
	
	
```
    $ cd eb_countrypopulation\countriesAPI    
    $ mvn package java -jar target/countriesAPI-0.0.1-SNAPSHOT.jar
```


Alternatively, you can run the app without packaging it using -   

``
	$ mvn spring-boot:run
``

Once the application runs you should see something like this

``
    2017-08-29 17:31:23.091  INFO 19387 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8090 (http)
``

``
    2017-08-29 17:31:23.097  INFO 19387 --- [           main] com.khoubyari.example.Application        : Started Application in 22.285 seconds (JVM running for 23.032)
``

The backend service will start at <http://localhost:8080>

**3. Run the frontend app using npm**	

```Bash
    $ cd eb_countrypopulation\countriesClient
    $ npm install
    $ npm start
```

Frontend  server will run on <http://localhost:4200>

---

## To view your H2 in-memory datbase

---

The 'test' profile runs on H2 in-memory database. To view and query the database you can browse to http://localhost:8080/h2-console. 
Default username is 'sa' with a blank password. Make sure you disable this in your production profiles. For more, see https://goo.gl/U8m62X



---

## API Specification

---

**API Basic Route** : <http://localhost:8080/>

**EndPoints**:

        1. /countries : GET - List of countries
        2. /countries/{id} : GET -- Country details
        3. /countries : POST -- Creates a country
        4. /countries : PUT -- Updates a country
        5. /countries: DELETE -- Deletes a country


## Changelog

---

* 0.0.1: start

---






