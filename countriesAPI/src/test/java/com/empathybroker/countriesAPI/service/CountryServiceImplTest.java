/**
 * Tests Package
 */
package com.empathybroker.countriesAPI.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.empathybroker.countriesAPI.model.Country;
import com.empathybroker.countriesAPI.repository.CountryRepository;
import com.empathybroker.countriesAPI.services.CountryServiceImpl;

/**
 * Class for unit testing of the service layer.
 * @author Noelia
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CountryServiceImplTest {

	@Mock
	private CountryRepository countryRepository;

	@InjectMocks
	private CountryServiceImpl countryService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllCountries() {
		List<Country> countriesList = new ArrayList<Country>();
		countriesList.add(new Country("Spain", 47000000));
		countriesList.add(new Country("Germany", 67890798));
		countriesList.add(new Country("France", 56908743));
		Mockito.when(countryRepository.findAll()).thenReturn(countriesList);

		List<Country> result = countryService.findAllCountries();
		assertEquals(3, result.size());
	}

	@Test
	public void testFindCountryById(){
        Country country = new Country(1,"Morocco", 150000);             
        Mockito.when(countryRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(country)); 
        Country result = countryService.findCountryById(1L);
        assertNotNull(result);
        assertEquals(1, result.getId());
        assertEquals("Morocco", result.getCountryName());
        assertEquals(150000,result.getPopulation());
}

	@Test
	public void testSaveCountry() {
		Country country = new Country("Toronto", 400000);
		Mockito.when(countryRepository.save(country)).thenReturn(country);
		Country result = countryService.saveCountry(country);
		assertEquals("Toronto", result.getCountryName());
		assertEquals(400000, result.getPopulation());
	}

	@Test
	public void testDeleteCountry() {
		Country country = new Country(2, "Malta", 150000);
		countryService.deleteCountry(country);
		Mockito.verify(countryRepository, Mockito.times(1)).delete(country);
	}
}
