/**
 * 
 */
package com.empathybroker.countriesAPI.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.empathybroker.countriesAPI.CountriesApiApplication;

/**
 * Class for unit tests of Controller's layer.
 * @author Noelia
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CountriesApiApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("test")
public class CountryControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

	}
	

	@Test
	public void verifyAllCountries() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/countries").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(4))).andDo(print());
	}

	@Test
	public void verifyCountryById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/countries/3").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").exists()).andExpect(jsonPath("$.countryName").exists())
				.andExpect(jsonPath("$.population").exists()).andExpect(jsonPath("$.id").value(3))
				.andExpect(jsonPath("$.countryName").value("Germany"))
				.andExpect(jsonPath("$.population").value(56790000)).andDo(print());
	}

	@Test
	public void verifyCountryById_invalidArgument() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/countries/f").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errorCode").value(400))
				.andExpect(jsonPath("$.message")
						.value("The request could not be understood by the server due to malformed syntax."))
				.andDo(print());
	}

	@Test
	public void verifyCountryById_invalidCountryId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/countries/0").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errorCode").value(404))
				.andExpect(jsonPath("$.message").value("Country doesn´t exist")).andDo(print());
	}

	@Test
	public void verifyCountryById_nullCountry() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/countries/6").accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errorCode").value(404))
				.andExpect(jsonPath("$.message").value("Country doesn´t exist")).andDo(print());
	}

	@Test
	public void verifyDeleteCountry() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/countries/").contentType(MediaType.APPLICATION_JSON)
				.content("{\"id\" : \"2\", \"countryName\" : \"Congo\" , \"population\" : \"1300\"}")
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.status").value(200))
				.andExpect(jsonPath("$.message").value("Country has been deleted")).andDo(print());
	}

	@Test
	public void verifySaveCountry() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/countries/").contentType(MediaType.APPLICATION_JSON)
				.content("{\"countryName\" : \"Brazil\", \"population\" : \"200000000\" }")
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.countryName").exists()).andExpect(jsonPath("$.population").exists())
				.andExpect(jsonPath("$.countryName").value("Brazil"))
				.andExpect(jsonPath("$.population").value(200000000)).andDo(print());
	}

	@Test
	public void verifyMalformedSaveCountry() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/countries/").contentType(MediaType.APPLICATION_JSON)
				.content("{ \"id\": \"8\", \"countryName\" : \"Brazil\", \"population\" : \"200000000\" }")
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.errorCode").value(404))
				.andExpect(jsonPath("$.message").value("Country malformed, id must not be defined")).andDo(print());
	}

	@Test
	public void verifyUpdateCountry() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/countries/").contentType(MediaType.APPLICATION_JSON)
				.content("{ \"id\": \"1\", \"countryName\" : \"Brazil\", \"population\" : \"23000000\" }")
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.id").exists())
				.andExpect(jsonPath("$.countryName").exists()).andExpect(jsonPath("$.population").exists())
				.andExpect(jsonPath("$.id").value(1)).andExpect(jsonPath("$.countryName").value("Brazil"))
				.andExpect(jsonPath("$.population").value(23000000)).andDo(print());
	}

	@Test
	public void verifyInvalidToDoUpdate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/countries/").contentType(MediaType.APPLICATION_JSON)
				.content("{ \"id\": \"8\", \"text\" : \"Honolulú\", \"population\" : \"1000\" }")
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.errorCode").value(404))
				.andExpect(jsonPath("$.message").value("Country to update doesn´t exist")).andDo(print());
	}
	
	

}
