/**
 * Services Package
 */
package com.empathybroker.countriesAPI.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empathybroker.countriesAPI.model.Country;
import com.empathybroker.countriesAPI.repository.CountryRepository;

/**
 * 
 * Implementation of the CountryService interface.
 * @author Noelia
 *
 */

@Service
public class CountryServiceImpl implements CountryService{

	/**
	 * ContryRepository object.
	 */
	@Autowired
	private CountryRepository countryRepository;
	
	
	/* (non-Javadoc)
	 * @see com.empathybroker.countriesAPI.services.CountryService#findAllCountries()
	 */
	@Override
	public List<Country> findAllCountries() {
		return (List<Country>) countryRepository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.empathybroker.countriesAPI.services.CountryService#findCountryById(long)
	 */
	@Override
	public Country findCountryById(long id) {
		Optional<Country> country = countryRepository.findById(id);
		if (country.isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see com.empathybroker.countriesAPI.services.CountryService#saveCountry(com.empathybroker.countriesAPI.model.Country)
	 */
	@Override
	public Country saveCountry(Country country) {
		return countryRepository.save(country);
	}

	/* (non-Javadoc)
	 * @see com.empathybroker.countriesAPI.services.CountryService#deleteCountry(com.empathybroker.countriesAPI.model.Country)
	 */
	@Override
	public void deleteCountry(Country country) {
		 countryRepository.delete(country);
		
	}

}
