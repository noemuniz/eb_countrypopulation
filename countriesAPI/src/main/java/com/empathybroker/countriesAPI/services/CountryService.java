/**
 * Services Package
 */
package com.empathybroker.countriesAPI.services;

import java.util.List;

import com.empathybroker.countriesAPI.model.Country;

/**
 * 
 * Service Interface
 * @author Noelia
 *
 */

public interface CountryService  {
	
	/**
	 * @return List of countries
	 */
	public List<Country> findAllCountries();
	
	/**
	 * Gets the country corresponding to the id passed as parameter.
	 * @param id, id of the country to find
	 * @return the country found, null if it was not found.
	 */
	public Country findCountryById(long id);
	
	/**
	 * Save(update/create) a country with the data passed as parameter.
	 * @param country, country details to stored
	 * @return the country created.
	 */
	public Country saveCountry(Country country);
	
	/**
	 * Deletes the country which matches with the country passed as parameter
	 * @param country the country to delete.
	 */
	public void deleteCountry(Country country);

}
