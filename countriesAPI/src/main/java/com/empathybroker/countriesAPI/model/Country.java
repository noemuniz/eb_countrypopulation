/**
 * Entities Package
 */
package com.empathybroker.countriesAPI.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

/**
 * 
 * Country Entity
 * @author Noelia
 *
 */
@Entity
@Table(name="country")
public class Country {

	/**
	 * Country id field.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/**
	 * Country Name
	 */
	@NonNull
	@Column(name="countryName")
	private String countryName;
	
	/**
	 * Country population
	 */
	@NonNull
	@Column(name="population")
	private long population;
	
	/**
	 * Default constructor
	 */
	public Country(){}
	
	/**
	 * Constructor
	 * @param countryName, name of the country
	 * @param population, country's population
	 */
	public Country(String countryName, long population) {
		this.countryName = countryName;
		this.population = population;
	}
	
	/**
	 * @param id, country id
	 * @param countryName, name of the country
	 * @param population, country's population
	 */
	public Country(long id, String countryName, long population) {
		this.id= id;
		this.countryName = countryName;
		this.population = population;
	}


	/**
	 * Gets the country id
	 * @return, id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Gets the country name
	 * @return String, country name
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * Sets the name of a country
	 * @param countryName, name to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * Gets the population
	 * @return long, population
	 */
	public long getPopulation() {
		return population;
	}

	/**
	 * Sets the population of a country
	 * @param population to set.
	 */
	public void setPopulation(long population) {
		this.population = population;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Country [id=" + id + ", countryName=" + countryName + ", population=" + population + "]";
	}
	
	
}
