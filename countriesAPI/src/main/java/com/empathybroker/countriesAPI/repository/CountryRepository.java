package com.empathybroker.countriesAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.empathybroker.countriesAPI.model.Country;

/**
 * 
 * JPA Repository for Country Entity class
 * @author Noelia
 *
 */
@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<Country, Long>{

}
