/**
 * Entities Package
 */
package com.empathybroker.countriesAPI.util;

/**
 * Response Class that stores a status  code and a messge associated.
 * @author Noelia
 *
 */
public class Response {

	/**
	 * Integer with the status code of the response
	 */
	private int status;
	/**
	 * Error Message Text
	 */
	private String message;

	/**
	 * Default constructor
	 */
	public Response() {
		super();
	}

	/**
	 * Creates a response entity
	 * @param status code
	 * @param message text
	 */
	public Response(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * @return status code
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return text message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
