/**
 * 
 */
package com.empathybroker.countriesAPI.util;

import com.empathybroker.countriesAPI.model.Country;

/**
 * Class for validate the country
 * @author Noelia
 *
 */
public class CountryValidator {
	
	/**
	 * @param country to validate
	 * @return true if it has an id > 0 , false if not.
	 */
	public static boolean validateCreateCountry(Country country){
		if (country.getId() > 0){
			return false;
		}
		return true;
}

}
