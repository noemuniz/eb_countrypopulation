/**
 * 
 */
package com.empathybroker.countriesAPI.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empathybroker.countriesAPI.exception.CountryException;
import com.empathybroker.countriesAPI.model.Country;
import com.empathybroker.countriesAPI.services.CountryService;
import com.empathybroker.countriesAPI.util.CountryValidator;
import com.empathybroker.countriesAPI.util.Response;

/**
 * API Controllers implementation
 * @author Noelia
 *
 */


@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600) //To enable CORS from the client
@RestController
@RequestMapping("/countries")
public class CountryController {
	
	private static final Logger logger = LoggerFactory.getLogger(CountryController.class);

	/**
	 * CountryService Object
	 */
	@Autowired
	CountryService countryService;

	/**
	 * @return <ResponseEntity<List<Country>>>, the list with the information of all countries
	 */
	@GetMapping
	public ResponseEntity<List<Country>> findAllCountries() {
		logger.info("Returning all countries");
		return new ResponseEntity<List<Country>>(countryService.findAllCountries(), HttpStatus.OK);
	}

	/**
	 * @param id, country identifier
	 * @return <ResponseEntity<Country>>, the country found.
	 * @throws <CountryException>, if the country is not found.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Country> findById(@PathVariable("id") long id) throws CountryException {
		logger.info("Country id to return " + id);
		Country country = countryService.findCountryById(id);
	
		if (country == null || country.getId() <= 0) {
			throw new CountryException("Country doesn´t exist");
		}
		return new ResponseEntity<Country>(countryService.findCountryById(id), HttpStatus.OK);

	}

	/**
	 * @param country, country with the details to save
	 * @return, the country saved.
	 */
	@PostMapping
	public ResponseEntity<Country> createCountry(@Valid @RequestBody Country country) throws CountryException {
		logger.info("Country to save " + country);
		if (!CountryValidator.validateCreateCountry(country)) {
			throw new CountryException("Country malformed, id must not be defined");
		}
		return new ResponseEntity<Country>(countryService.saveCountry(country), HttpStatus.OK);

	}

	/**
	 * @param country country with the information to update
	 * @return country updated
	 * @throws CountryException, if the country is not found into the collection
	 */
	@PutMapping
	public ResponseEntity<Country> UpdateCountry(@Valid @RequestBody Country country) throws CountryException {

		logger.info("Country to update" + country);
		Country countryData = countryService.findCountryById(country.getId());

		if (countryData != null) {
			Country _country = countryData;
			_country.setCountryName(country.getCountryName());
			_country.setPopulation(country.getPopulation());
			return new ResponseEntity<>(countryService.saveCountry(_country), HttpStatus.OK);
		} else {
			throw new CountryException("Country to update doesn´t exist");
		}
	}

	/**
	 * @param country to be delete
	 * @return <ResponseEntity<Response>> with the response status and a message
	 * @throws CountryException, if the country to delete is not found into the collection.
	 */
	@DeleteMapping
	public ResponseEntity<Response> deleteCountry(@Valid @RequestBody Country country) throws CountryException {
		logger.info("Country to delete - " + country);
		Country _country = countryService.findCountryById(country.getId());
		if (country == null || CountryValidator.validateCreateCountry(country)) {
			throw new CountryException("Country to delete doesn´t exist");
		}
		countryService.deleteCountry(_country);
		return new ResponseEntity<Response>(new Response(HttpStatus.OK.value(), "Country has been deleted"),
				HttpStatus.OK);

	}
	
	

}
