/**
 * 
 */
package com.empathybroker.countriesAPI.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.empathybroker.countriesAPI.util.ErrorResponse;

/**
 * 
 * Class that handles the CountryException error and the Exception error.
 * 
 * @author Noelia
 *
 */

@ControllerAdvice
public class RestExceptionHandler {
	/**
	 * Handles the CountryException exception
	 * 
	 * @param ex,
	 *            <Exception>
	 * @return <ResponseEntity<ErrorResponse>>
	 */
	@ExceptionHandler(CountryException.class)
	public ResponseEntity<ErrorResponse> exceptionCountryHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.NOT_FOUND.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles the Exception error
	 * 
	 * @param ex,
	 *            <Exception>
	 * @return <ResponseEntity<ErrorResponse>>
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage("The request could not be understood by the server due to malformed syntax.");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
	}
}
