/**
 * 
 */
package com.empathybroker.countriesAPI.exception;

/**
 * 
 * Country Exception class, stores the information associated to a CountryException error.
 * @author Noelia
 *
 */
public class CountryException extends Exception {
	private static final long serialVersionUID = 1L;
	/**
	 * errorMessage
	 */
	private String errorMessage;

	/**
	 * @return <String> with the message error.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Constructor, creates a CountryException with the errorMessage passed as parameter
	 * @param errorMessage <String> message error.
	 */
	public CountryException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	/**
	 * Default constructor.
	 */
	public CountryException() {
		super();
	}

}
