package com.empathybroker.countriesAPI;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.empathybroker.countriesAPI.model.Country;
import com.empathybroker.countriesAPI.repository.CountryRepository;

@SpringBootApplication
public class CountriesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountriesApiApplication.class, args);
	}
	

	@Bean
	@Profile("test")
	public CommandLineRunner setupTest(CountryRepository countryRepository) {
		return (args) -> {
			countryRepository.deleteAll();
			countryRepository.save(new Country(1,"Afganistan", 345678));
			countryRepository.save(new Country(2,"Congo", 1300));
			countryRepository.save(new Country(3,"Germany", 56790000));
			countryRepository.save(new Country(4,"Malta", 200000));
		};
	}
		
		@Bean
		@Profile("!test")
		public CommandLineRunner setupProduction(CountryRepository countryRepository) {
			return (args) -> {
				countryRepository.deleteAll();
				countryRepository.save(new Country(1,"Morocco", 100));
				countryRepository.save(new Country(2,"Congo", 2300));
			};	
}
	
}
